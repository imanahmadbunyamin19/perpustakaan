<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuController extends Controller
{
    // index
    public function index()
    {
        $buku = DB::table('buku')->get();
        return view('buku.index', compact('buku'));
    }

    // ke halaman create / add buku
    public function create()
    {
        return view('buku.create');
    }

    // proses simpan data buku baru
    public function store(Request $request)
    {
        // validasi input dulu
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun_terbit' => 'required',
            'lantai' => 'required'
        ]);

        // simpan 
        $query = DB::table('buku')->insert([
            "judul_buku" => $request["judul"],
            "nama_pengarang" => $request["pengarang"],
            "penerbit" => $request["penerbit"],
            "tahun_terbit" => $request["tahun_terbit"],
            "nomor_lantai" => $request["lantai"],
            "status" => "Tersedia", //status Tersedia berarti buku ini bisa dipinjam
        ]);
        return redirect('/buku');
    }

    // menampilkan data buku
    public function show($id)
    {
        $buku = DB::table('buku')->where('id', $id)->first();
        return view('buku.show', compact('buku'));
    }
 
    // untuk edit data buku
    public function edit($id)
    {
        $buku = DB::table('buku')->where('id', $id)->first();
        return view('buku.edit', compact('buku'));
    }

    // untuk melakukan update data buku 
    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun_terbit' => 'required',
            'lantai' => 'required'
        ]);

        $query = DB::table('buku')
            ->where('id', $id)
            ->update([
                "judul_buku" => $request["judul"],
                "nama_pengarang" => $request["pengarang"],
                "penerbit" => $request["penerbit"],
                "tahun_terbit" => $request["tahun_terbit"],
                "nomor_lantai" => $request["lantai"]
            ]);
        return redirect('/buku');
    }
    
    // menghapus data buku
    public function destroy($id)
    {
        $query = DB::table('buku')->where('id', $id)->delete();
        return redirect('/buku');
    }

    // list buku di root / home
    public function list()
    {
        $buku = DB::table('buku')->where('status','Tersedia')->get();
        return view('buku.list', compact('buku'));
    }
}
