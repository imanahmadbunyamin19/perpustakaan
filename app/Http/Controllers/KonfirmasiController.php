<?php

namespace App\Http\Controllers;

use App\Models\Peminjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KonfirmasiController extends Controller
{
    // index
    public function index()
    {
        $peminjaman = Peminjaman::where('status','Menunggu Konfirmasi')->get();

        return view('peminjaman.konfirmasi', compact('peminjaman'));
    }

    // Konfirmasi peminjaman buku
    public function konfirm($id)
    {
        //update status peminjaman
        DB::table('peminjaman')
            ->where('id', $id)
            ->update([
                "status" => "Sedang Dipinjam"
        ]);

        return redirect('/mahasiswa');
    }
}
