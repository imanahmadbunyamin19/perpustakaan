<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
     // index
     public function index()
     {
         $mahasiswa = DB::table('mahasiswa')->get();
         return view('mahasiswa.index', compact('mahasiswa'));
     }


    // menampilkan data mahasiswa
    public function show($id)
    {
        $mahasiswa = DB::table('mahasiswa')->where('id', $id)->first();
        return view('mahasiswa.show', compact('mahasiswa'));
    }
 
    // untuk edit data mahasiswa
    public function edit($id)
    {
        $mahasiswa = DB::table('mahasiswa')->where('id', $id)->first();
        return view('mahasiswa.edit', compact('mahasiswa'));
    }

    // untuk melakukan update data mahasiswa 
    public function update($id, Request $request)
    {
        $request->validate([
            'npm' => 'required',
            'nama' => 'required',
        ]);

        $query = DB::table('mahasiswa')
            ->where('id', $id)
            ->update([
                "npm" => $request["npm"],
                "nama" => $request["nama"],
            ]);
        return redirect('/mahasiswa');
    }
    
    // menghapus data mahasiswa
    public function destroy($id)
    {
        $query = DB::table('mahasiswa')->where('id', $id)->delete();
        return redirect('/mahasiswa');
    }
}
