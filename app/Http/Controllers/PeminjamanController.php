<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use App\Models\Peminjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class PeminjamanController extends Controller
{
    // index
    public function index()
    {
        $peminjaman = Peminjaman::where('status','Sedang dipinjam')->get();

        return view('peminjaman.index', compact('peminjaman'));
    }

    // tambah ke cart
    public function addToCart($id_buku)
    {
        $cart = session("cart");

        // cek buku ada atau tidak
        $buku = DB::table('buku')->where('id', $id_buku)->first();

        if (!$buku) {
        abort(404);
        }

        // memasukkan ke cart
        $cart[$id_buku] = [
            "id" => $buku->id,
            "judul_buku" => $buku->judul_buku
        ];
        
        session(["cart" => $cart]);

        return redirect('/cart')->with('Sukses', 'Buku sudah ditambahkan!');
    }

    // hapus dari cart
    public function deleteFromCart($id_buku)
    {

    $cart = session("cart");
    unset($cart[$id_buku]);
    session(["cart" => $cart]);

    return redirect('/cart')->with('Sukses', 'Buku sudah dihapus !');
    }

    public function cart(){
        $cart = session("cart");
        return view('peminjaman.cart')->with("cart",$cart);
    }

    // Transaksi ke tabel peminjaman dan detail, update status buku
    public function transaksiPinjam()
    {
        // session()->forget("cart");
        $user = Auth::user();
        $cart = session("cart");
        
        //ambil tanggal
        $current_date_time = Carbon::now()->toDateTimeString();
        
        //hitung tanggal pengembalian. dibuat default 5 hari 
        $tgl_pengembalian  =date('Y-m-d', strtotime('+5 days', strtotime($current_date_time)));

        // Insert ke table peminjaman
        DB::table('peminjaman')->insert(
            [
                'tgl_peminjaman' =>  $current_date_time,
                'tgl_batas_pengembalian' =>  $tgl_pengembalian,
                'mahasiswa_id' =>$user['id'],
                'status' => 'Menunggu Konfirmasi'
            ]);

        // Insert ke table detail
        foreach($cart as $ct => $value){
            DB::table('detail')->insert([
                'peminjaman_id' => $value['id'],
                'buku_id' => $ct,
            ]);

            // dd($ct);
            DB::table('buku')
            ->where('id', $ct)
            ->update([
                "status" => "Menunggu Konfirmasi"
            ]);

        return redirect('/');

        }

        //clear session cart
        session()->forget("cart");

        return redirect('/cart');
    }

  }
