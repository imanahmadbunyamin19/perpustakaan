<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PengembalianController extends Controller
{
    // Pengembalian buku
    public function konfirm($id)
    {
        //update status peminjaman
        DB::table('peminjaman')
            ->where('id', $id)
            ->update([
                "status" => "Tersedia"
        ]);

        return redirect('/peminjaman');
    }
}
