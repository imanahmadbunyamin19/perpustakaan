<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = 'detail';

    public function peminjaman()
    {
        return $this->belongsTo('App\Models\Peminjaman');
    }
}
