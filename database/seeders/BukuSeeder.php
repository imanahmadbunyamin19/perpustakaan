<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('buku')->insert([
            'judul_buku' => 'Ingin Naik Haji',
            'nama_pengarang' => 'Iman Ahmad Bunyamin',
            'penerbit' => 'IA Corps',
            'tahun_terbit' => '2023',
            'nomor_lantai' => '02',
            'status' => 'Tersedia',
        ]);

        DB::table('buku')->insert([
            'judul_buku' => 'Tukang Coding Naik Haji',
            'nama_pengarang' => 'Ahmad Iman',
            'penerbit' => 'IA Corps',
            'tahun_terbit' => '2024',
            'nomor_lantai' => '01',
            'status' => 'Tersedia',
        ]);
    }
}
