<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Mahasiswa')->insert([
            'npm' => '12345678',
            'nama' => 'Admin',
            'user_id' => 1
        ]);
    }
}
