<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PERPUSTAKAAN | Halaman Register</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="#" class="h1"><b>PERPUSTAKAAN </b></a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Pendaftaran Akun</p>

      <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="input-group mb-3">
          <input type="text" name="npm" class="form-control" maxlength="8" placeholder="Nomor Pokok Mahasiswa" value="{{old('npm')}}" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa fa-university"></span>
            </div>
          </div>
        </div>
        @error('npm')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
        @enderror

        <div class="input-group mb-3">
          <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" value="{{old('name')}}">
          <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-user"></span>
            </div> 
          </div>
        </div>
        @error('name')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror

        <div class="input-group mb-3">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
            <div class="input-group-append">
              <div class="input-group-text">
                  <span class="fas fa fa-envelope"></span>
              </div> 
            </div>
        </div>
        @error('email')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        @error('password')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
        @enderror

        <div class="input-group mb-3">
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Retype Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        @error('password_confirmation')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">
              {{ __('Register') }}
            </button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mt-2 text-left">
        Sudah punya akun ? <a href="/login">Login disini </a>
      </p>

    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
</body>
</html>
