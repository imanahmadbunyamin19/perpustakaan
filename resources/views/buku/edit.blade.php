@section('judul')
Data Buku
@endsection

@extends('template.template')

@push('script')
<script>
    // Input only number
    $(function() {
      $('#only-number').on('keydown', '#phone', function(e){
          -1!==$
          .inArray(e.keyCode,[46,8,9,27,13,110,190]) || /65|67|86|88/
          .test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey)
          || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey|| 48 > e.keyCode || 57 < e.keyCode)
          && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
      });
    })
</script>
@endpush

@push('style')

@endpush

@section('content')
<div class="mx-2">
        <form action="/buku/{{$buku->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">Judul Buku </label>
                <input type="text" class="form-control" name="judul" id="judul" value="{{$buku->judul_buku}}" autofocus>
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="pengarang">Nama Pengarang </label>
                <input type="text" class="form-control" name="pengarang" id="pengarang" value="{{$buku->nama_pengarang}}">
                @error('pengarang')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="penerbit">Penerbit </label>
                <input type="text" class="form-control" name="penerbit" id="penerbit" value="{{$buku->penerbit}}">
                @error('penerbit')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <div class="form-group" id="only-number">
                <label for="tahun_terbit">Tahun Terbit</label>
                <input type="number" class="form-control" name="tahun_terbit" id="tahun_terbit" value="{{$buku->tahun_terbit}}" maxlength="4">
                @error('tahun_terbit')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="lantai">Nomor Lantai </label>
                <input type="text" class="form-control" name="lantai" id="lantai" value="{{$buku->nomor_lantai}}">
                @error('lantai')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>          

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
@endsection
