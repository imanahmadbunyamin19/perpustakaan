@section('judul')
Data Buku
@endsection

@extends('template.template')

@push('script')

    {{-- Library Data table  --}}
    <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
    
@endpush

@push('style')
    {{-- datatables --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

<div class="mx-2">
    <div class="card">
        <div class="card-body">
            <table id="example1" class="table table-striped">
                <thead>
                    <tr>
                        <th width="1%">No</th>
                        <th>Judul Buku</th>
                        <th>Nama Pengarang</th>
                        <th>Penerbit</th>
                        <th>Tahun Terbit</th>
                        <th>Status</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($buku as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</th>
                            <td>{{$value->judul_buku}}</td>
                            <td>{{$value->nama_pengarang}}</td>
                            <td>{{$value->penerbit}}</td>
                            <td>{{$value->tahun_terbit}}</td>
                            <td>{{$value->status}}</td>
                            <td>
                                <a class="btn-sm btn-primary" href="{{ url('addtocart/'.$value->id) }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" class="text-center">Belum ada data buku</td>
                        </tr>  
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection