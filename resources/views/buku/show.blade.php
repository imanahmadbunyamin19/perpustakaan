@section('judul')
Detail Buku
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')
<div class="mx-2">
    <div class="form-group">
        <label for="judul">Judul Buku </label>
        <input type="text" class="form-control" name="judul" id="judul" value="{{$buku->judul_buku}}" readonly>
    </div>

    <div class="form-group">
        <label for="pengarang">Nama Pengarang </label>
        <input type="text" class="form-control" name="pengarang" id="pengarang" value="{{$buku->nama_pengarang}}" readonly>
    </div>

    <div class="form-group">
        <label for="penerbit">Penerbit </label>
        <input type="text" class="form-control" name="penerbit" id="penerbit" value="{{$buku->penerbit}}" readonly>
    </div>
    
    <div class="form-group" id="only-number">
        <label for="tahun_terbit">Tahun Terbit</label>
        <input type="number" class="form-control" name="tahun_terbit" id="tahun_terbit" value="{{$buku->tahun_terbit}}" readonly>
    </div>

    <div class="form-group">
        <label for="lantai">Nomor Lantai </label>
        <input type="text" class="form-control" name="lantai" id="lantai" value="{{$buku->nomor_lantai}}" readonly>
    </div>          
    
</div>
@endsection
