@section('judul')
Data Mahasiswa
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')

<div class="mx-2">
        <form action="/mahasiswa/{{$mahasiswa->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="npm">NPM</label>
                <input type="text" class="form-control" name="npm" id="npm" maxlength="8" value="{{$mahasiswa->npm}}" autofocus>
                @error('npm')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="nama">Nama Lengkap </label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{$mahasiswa->nama}}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>   

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
@endsection
