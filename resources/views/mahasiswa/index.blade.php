@section('judul')
Data Mahasiswa
@endsection

@extends('template.template')

@push('script')
    {{-- Library Data table  --}}
    <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>

    {{-- Library Sweet alert --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
     
     $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Apakah benar mahasiswa ini akan dihapus?`,
              text: " tindakan ini tidak dapat dikembalikan ",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
  
    </script>

@endpush

@push('style')
{{-- library, css, etc, Here! --}}

    {{-- datatables --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<div class="mx-2">
    <div class="card">
        <div class="card-body">
        <table id="example1" class="table table-striped">
            <thead>
                <tr>
                    <th width="1%">No</th>
                    <th>NPM</th>
                    <th>Nama Mahasiswa</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($mahasiswa as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->npm}}</td>
                        <td>{{$value->nama}}</td>
                        <td >
                            <form action="/mahasiswa/{{$value->id}}" method="POST">
                                <a href="/mahasiswa/{{$value->id}}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                                <a href="/mahasiswa/{{$value->id}}/edit" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>

                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger show_confirm" data-toggle="tooltip" title='Delete' >
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </form>
                        </td>  
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center">Belum ada data mahasiswa</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        </div>
    </div>
</div>


@endsection