@section('judul')
 Cart
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')
<div class="mx-2">
    <div class="col-6">
        <form action="/transaksipinjam" method="POST">
            @csrf
            <a href="{{ url('/') }}" class="btn btn-primary btn-sm mb-2" title="Cari Buku Lagi"><i class="fa fa-plus"> Tambah Buku</i></a>
            <div class="card">
                <div class="card-body">        
                    @if (empty($cart) || count($cart) == 0)
                        Cart Masih Kosong
                    @else
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="90%">Judul Buku</th>
                                    <th width="5%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no=1;   
                                @endphp   
                                
                                @foreach ($cart as $ct => $value)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $value["judul_buku"] }}</td>
                                        <td>
                                            <a href="{{ url('deletefromcart/'.$value["id"]) }}" class="btn btn-danger btn-sm" title="Hapus Dari Cart"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-end mt-3">
                            <button type="submit" class="btn btn-primary ">Simpan</button>
                        </div>
                    @endif 
                </div>
            </div>
           
        </form>
    </div>
</div>


@endsection