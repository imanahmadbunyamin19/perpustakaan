@section('judul')
Riwayat Peminjaman Buku
@endsection

@extends('template.template')

@push('script')
    {{-- Library Data table  --}}
    <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>

    {{-- Library Sweet alert --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
     
     $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Apakah benar peminjaman ini akan dihapus?`,
              text: " tindakan ini tidak dapat dikembalikan ",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
  
    </script>

@endpush

@push('style')
{{-- library, css, etc, Here! --}}

    {{-- datatables --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<div class="mx-2">
    <div class="card">
        <div class="card-body">
        <table id="example1" class="table table-striped">
            <thead>
                <tr>
                    <th width="1%">No</th>
                    <th>Nama Mahasiswa</th>
                    <th>Tanggal Pinjam</th>
                    <th>Batas Pengembalian</th>
                    <th>Status Pinjam</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($peminjaman as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->mahasiswa->nama}}</td>
                        <td>{{$value->tgl_peminjaman}}</td>
                        <td>{{$value->tgl_batas_pengembalian}}</td>
                        <td>{{$value->status}}</td>
                        <td >
                            <a href="/pengembalian/{{$value->id}}" class="btn btn-danger btn-sm"><i class="fa fa-check"> Kembali</i></a>
                        </td>  
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center">Belum ada data peminjaman</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        </div>
    </div>
</div>


@endsection