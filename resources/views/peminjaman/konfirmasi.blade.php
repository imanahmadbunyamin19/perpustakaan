@section('judul')
Menunggu Konfirmasi
@endsection

@extends('template.template')

@push('script')

    {{-- Library Sweet alert --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
     
     $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Apakah benar peminjaman ini akan dihapus?`,
              text: " tindakan ini tidak dapat dikembalikan ",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
  
    </script>

@endpush

@push('style')

@endpush

@section('content')
<div class="mx-2">
    <div class="card">
        <div class="card-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nama Mahasiswa</th>
                    <th>Tanggal Pengajuan</th>
                    <th>Status</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($peminjaman as $key=>$value)
                    <tr>
                        <td>{{$value->mahasiswa->nama}}</td>
                        <td>{{$value->tgl_peminjaman}}</td>
                        <td> <b style="color:blue"> {{$value->status}} </b></td>
                        <td >
                            <a href="/konfirmasi/{{$value->id}}" class="btn btn-primary btn-sm" title="Konfirmasi"><i class="fa fa-check"> Konfirmasi</i></a>
                        </td>  
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center">Belum ada data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        </div>
    </div>
</div>


@endsection