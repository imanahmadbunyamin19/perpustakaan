<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div>
      <div class="brand-link" >
        <p style="text-align:center"><strong> SISTEM INFORMASI <br> PERPUSTAKAAN</strong></p>
      </div>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            @auth
              <a href="#" class="d-block"> <strong> {{ Auth::user()->name }} </strong> </a>
            @endauth

            @guest
              <a href="#" class="d-block">Belum Login</a>
            @endguest

        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
            <li class="nav-item">
              <a href="/" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p> Dashboard</p>
              </a>
            </li> 
            @guest
              <li class="nav-item">
              <a href="/login" class="nav-link">
                  <i class="nav-icon fas fa-folder-open"></i>
                  <p> Login</p>
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
                @csrf 
              </form>
            </li>
          @endguest

          @auth
            @if (Auth::user()->rule === 'admin' )
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa fa-cogs"></i> 
                  <p>
                    Data Master
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/buku" class="nav-link">
                      <i class="far fa fa-book nav-icon"></i>
                      <p>Data Buku</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="mahasiswa" class="nav-link">
                      <i class="far fa fa-users nav-icon"></i>
                      <p>Data Mahasiswa</p>
                    </a>
                  </li>
                </ul>
              </li>          
              <li class="nav-item">
                <a href="/konfirmasi" class="nav-link">
                  <i class="nav-icon fas fa fa-clock"></i>
                  <p>
                    Menunggu Konfirmasi
                  </p>
                </a>
              </li>    
              <li class="nav-item">
                <a href="/peminjaman" class="nav-link">
                  <i class="nav-icon fas fa fa-calendar"></i>
                  <p>
                    Riwayat
                  </p>
                </a>
              </li>
            @endif 
            
            {{-- button logout --}}
            <li class="nav-item">
              <a href="{{ route('logout') }}" class="nav-link"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  <i class="nav-icon fas fa fa-times-circle"></i>
                  <p> Logout</p>
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
                @csrf 
              </form>
            </li>
            
          @endauth


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>