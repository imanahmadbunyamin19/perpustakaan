<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\KonfirmasiController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\PeminjamanController;
use App\Http\Controllers\PengembalianController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// root
Route::get('/', [BukuController::class,'list']);

// route untuk data buku 
Route::get('/buku', [BukuController::class,'index'])->middleware('auth');
Route::get('/buku/create', [BukuController::class,'create'])->middleware('auth');
Route::post('/buku', [BukuController::class,'store'])->middleware('auth');
Route::get('/buku/{buku_id}', [BukuController::class,'show'])->middleware('auth');
Route::get('/buku/{buku_id}/edit', [BukuController::class,'edit'])->middleware('auth');
Route::put('/buku/{buku_id}', [BukuController::class,'update'])->middleware('auth');
Route::delete('/buku/{buku_id}', [BukuController::class,'destroy'])->middleware('auth');

// route untuk data mahasiswa atau anggota 
Route::get('/mahasiswa', [MahasiswaController::class,'index'])->middleware('auth');
Route::get('/mahasiswa/create', [MahasiswaController::class,'create'])->middleware('auth');
Route::post('/mahasiswa', [MahasiswaController::class,'store'])->middleware('auth');
Route::get('/mahasiswa/{mahasiswa_id}', [MahasiswaController::class,'show'])->middleware('auth');
Route::get('/mahasiswa/{mahasiswa_id}/edit', [MahasiswaController::class,'edit'])->middleware('auth');
Route::put('/mahasiswa/{mahasiswa_id}', [MahasiswaController::class,'update'])->middleware('auth');
Route::delete('/mahasiswa/{mahasiswa_id}', [MahasiswaController::class,'destroy'])->middleware('auth');


// route untuk data peminjaman
Route::get('/peminjaman', [PeminjamanController::class,'index'])->middleware('auth');
Route::get('/peminjaman/create', [PeminjamanController::class,'create'])->middleware('auth');
Route::post('/peminjaman', [PeminjamanController::class,'store'])->middleware('auth');
Route::get('/peminjaman/{peminjaman_id}', [PeminjamanController::class,'show'])->middleware('auth');
Route::get('/peminjaman/{peminjaman_id}/edit', [PeminjamanController::class,'edit'])->middleware('auth');
Route::put('/peminjaman/{peminjaman_id}', [PeminjamanController::class,'update'])->middleware('auth');
Route::delete('/peminjaman/{peminjaman_id}', [PeminjamanController::class,'destroy'])->middleware('auth');

//cart
Route::get('/addtocart/{id}', [PeminjamanController::class,'addToCart'])->middleware('auth');
Route::get('/deletefromcart/{id}', [PeminjamanController::class,'deleteFromCart'])->middleware('auth');
Route::get('/cart', [PeminjamanController::class,'cart'])->middleware('auth');

//transaksi dan konfirmasi
Route::post('/transaksipinjam', [PeminjamanController::class,'transaksiPinjam'])->middleware('auth');
Route::get('/konfirmasi', [KonfirmasiController::class,'index'])->middleware('auth');
Route::get('/konfirmasi/{id}', [KonfirmasiController::class,'konfirm'])->middleware('auth');

//pengembalian Buku
Route::get('/pengembalian/{id}', [PengembalianController::class,'konfirm'])->middleware('auth');

Auth::routes();

